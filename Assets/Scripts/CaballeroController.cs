using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class CaballeroController : MonoBehaviour
{    
    private Rigidbody2D rb;
    private Animator animator;
    private SpriteRenderer sr;
    void Start()
    {
        rb = GetComponent<Rigidbody2D>();
        animator = GetComponent<Animator>();
        sr = GetComponent<SpriteRenderer>();
    }

    // Update is called once per frame
    void Update()
    {
        rb.velocity = new Vector2(0, rb.velocity.y);
        animator.SetInteger("Estado", 0);
        if (Input.GetKey(KeyCode.RightArrow))
        {
            rb.velocity = new Vector2(10, rb.velocity.y); 
            sr.flipX = false;
            animator.SetInteger("Estado",1);
        }
        if (Input.GetKey(KeyCode.LeftArrow))
        {
            rb.velocity = new Vector2(-10, rb.velocity.y); 
            sr.flipX = true;
            animator.SetInteger("Estado",1);
        }
        if (Input.GetKey(KeyCode.RightArrow) && Input.GetKey(KeyCode.X))
        {
            rb.velocity = new Vector2(rb.velocity.x + 5, rb.velocity.y); 
            sr.flipX = false;
            animator.SetInteger("Estado",2);
        }
        
        if (Input.GetKey(KeyCode.LeftArrow) && Input.GetKey(KeyCode.X))
        {
            rb.velocity = new Vector2(rb.velocity.x - 5, rb.velocity.y); 
            sr.flipX = true;
            animator.SetInteger("Estado",2);
        }

        if (Input.GetKeyUp(KeyCode.Space))
        {
            rb.AddForce(Vector2.up * 20, ForceMode2D.Impulse);
            animator.SetInteger("Estado",3);
        }
        if (Input.GetKey(KeyCode.Z))
        {
            animator.SetInteger("Estado",4);
        }
    }
}
